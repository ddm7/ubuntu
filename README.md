# linux-apt-get-aliyun

### 介绍
apt-get阿里云镜像，不用手动vi去编辑sources.list。


### 使用说明

#### 1.  先克隆到linux系统中

`git clone https://gitee.com/isfive/linux-apt-get-aliyun.git`

#### 2.  备份原来的sources.list

`cp /etc/apt/sources.list /etc/apt/sources.list.bak`

#### 3.  删除原来的sources.list

`sudo rm /etc/apt/sources.list`



#### 4. 首先进入linux-apt-get-aliyun目录下
`cd linux-apt-get-aliyun`

#### 5. 查看支持的版本信息
`ls`
```
huangwz@cee-pc MINGW64 ~/Desktop/linux-apt-get-aliyun (master)
$ ls
12.04LTS  14.04.5-LTS  16.04  18.04  19.04  20.04-aliyun  20.04LTS  LICENSE  README.en.md  README.md
```
#### 6. 查看版本信息，根据ubuntu版本进入相应的文件夹，例如我是16.04

`cd ./16.04`

#### 7. 复制sources.list文件到/etc/apt文件夹
`sudo cp ./sources.list /etc/apt/sources.list`

#### 8. 更新apt-get
`sudo apt-get update`


